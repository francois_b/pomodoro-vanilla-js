import './style.css'

const pomodoroTimer = document.querySelector('#pomodoro-timer')

const startButton = document.querySelector('#pomodoro-start')
const pauseButton = document.querySelector('#pomodoro-pause')
const stopButton = document.querySelector('#pomodoro-stop')

//Start
startButton.addEventListener('click', () => {
  toggleClock()
})

//Pause
pauseButton.addEventListener('click', () => {
  toggleClock()
})

//Stop
stopButton.addEventListener('click', () => {
  toggleClock(true);
})

let isClockRunning = false

let workSessionDuration = 1500
let currenTimeLeftInSession = 1500

let breakSessionDuration = 300

const toggleClock = (reset) => {
  if (reset) {
    //STOP THE TIMER
  } else {
    if (isClockRunning === true) {
      // PAUSE THE TIMER
      isClockRunning = false
      clearInterval(clockTimer)
    } else {
      // START THE TIMER
      isClockRunning = true
      clockTimer = setInterval(() => {
        currentTimeLeftSession--
        displayCurrentTimeLeftInSession()
      }, 1000)
    }
  }
}

const displayCurrentTimeLeftInSession = () => {
  const secondsLeft = currentTimeLeftSession
  let result = ''
  const seconds = secondsLeft % 60
  const minutes = parseInt(secondsLeft / 60) % 60
  let hours = parseInt(secondsLeft / 3600)
  function addLeadingZeroes(time) {
    return time < 10 ? `0${time}`: time
  }
  if (hours > 0) result +=`${hours}`
  result += `${addLeadingZeroes(minutes)}:${addLeadingZeroes(seconds)}}`
  pomodoroTimer.innerText = result.toString()
}